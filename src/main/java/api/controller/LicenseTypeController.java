package api.controller;

import api.entity.LicenseType;
import api.repository.LicenseTypeRepository;
import api.rest.RestImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/license-type")
public class LicenseTypeController extends RestImplementation<LicenseTypeRepository, LicenseType> {

    private LicenseTypeRepository licenseTypeRepository;

    public LicenseTypeController(
            LicenseTypeRepository licenseTypeRepository
    ) {
        super(licenseTypeRepository);
        this.licenseTypeRepository = licenseTypeRepository;
    }

}
