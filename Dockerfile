FROM gradle:4.2.1-jdk8-alpine
USER root
RUN mkdir -p /usr/src
WORKDIR /usr/src
COPY . /usr/src/

RUN gradle build --stacktrace

WORKDIR /usr/src/build/libs

RUN chmod 777 -R license-manager-api-latest.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","license-manager-api-latest.jar"]
#port
EXPOSE 8080
